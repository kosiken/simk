import sys
import getopt
import gtk
class MessageBox(gtk.Dialog):
 def __init__(self, message="", buttons=(), pixmap=None,modal= True):
  gtk.Dialog.__init__(self)
  self.connect("destroy", self.quit)
  self.connect("delete_event", self.quit)
  if modal:
   self.set_modal(True)
hbox = gtk.HBox(spacing=5)
hbox.set_border_width(5)
self.vbox.pack_start(hbox)
hbox.show()
if pixmap:
self.realize()
pixmap = Pixmap(self, pixmap)
hbox.pack_start(pixmap, expand=False)
pixmap.show()
label = gtk.Label(message)
hbox.pack_start(label)
label.show()
for text in buttons:
b = gtk.Button(text)
b.set_flags(gtk.CAN_DEFAULT)
b.set_data("user_data", text)
b.connect("clicked", self.click)
self.action_area.pack_start(b)
b.show()
self.ret = None
def quit(self, *args):
self.hide()
self.destroy()
gtk.main_quit()
def click(self, button):
self.ret = button.get_data("user_data")
self.quit()
# create a message box, and return which button was pressed
def message_box(title="Message Box", message="", buttons=(),
pixmap=None,
modal= True):
win = MessageBox(message, buttons, pixmap=pixmap, modal=modal)
win.set_title(title)