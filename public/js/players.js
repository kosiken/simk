
class Player {
  constructor (
    firstName, lastName,
    rating, age, position,
    height, teamId, teamName, ratingSpec, id
  ) {
    this.id = id
    this.firstName = firstName
    this.lastName = lastName
    this.rating = rating
    this.age = age
    this.position = position
    this.height = height
    this.teamId = teamId
    this.teamName = teamName
    this.ratingSpec = ratingSpec
    let okey
    if (rating > 87) okey = 100
    else if (rating > 80)okey = 80
    else if (rating > 70)okey = 50
    else okey = 20
    this.contract = `${Math.round(rating / 20)}years<br> $${Math.round((okey / 40) * rating)}million`

    this.assistsL = []
    this.pointsL = []
    this.reboundsL = []
    this.blocksL = []
    this.stealsL = []
    this.statsSpec = { PPG: '0.00',
      RPG: `0.00`,
      APG: `0.00`,
      SPG: `0.00`,
      BPG: `0.00` }
  }
  tea (t) {
    this.team = t
  }

  addMins (mins) {
    this.mins = mins
  }

  playing () {
    if (this.mins !== 'DNP') {
      this.points = calcStat(this.ratingSpec, this.mins).points
      this.rebounds = calcStat(this.ratingSpec, this.mins).rebounds
      this.assists = calcStat(this.ratingSpec, this.mins).assists
      this.steals = calcStat(this.ratingSpec, this.mins).steals
      this.blocks = calcStat(this.ratingSpec, this.mins).blocks

      this.assistsL.push(this.assists)
      this.pointsL.push(this.points)
      this.reboundsL.push(this.rebounds)
      this.stealsL.push(this.steals)
      this.blocksL.push(this.blocks)
      this.statsSpec = { PPG: average(this.pointsL),
        RPG: average(this.reboundsL),
        APG: average(this.assistsL),
        SPG: average(this.stealsL),
        BPG: average(this.blocksL) }
      return {
        points: this.points,
        rebounds: this.rebounds,
        assists: this.assists,
        steals: this.steals,
        blocks: this.blocks
      }

      //  console.log( this.points );
    } else { return 0 }
  } 
 
  static fromJSON (data) {
    const beek = new Player(data.firstName, data.lastName, data.rating, data.age, data.position,
      data.height, data.teamId, data.teamName, data.ratingSpec, data.id)
    beek.assistsL = data.assistsL
    beek.pointsL = data.pointsL
    beek.reboundsL = data.reboundsL
    beek.blocksL = data.blocksL
    beek.stealsL = data.stealsL
    beek.statsSpec = { PPG: average(data.pointsL),
    RPG: average(data.reboundsL),
    APG: average(data.assistsL),
    SPG: average(data.stealsL),
    BPG: average(data.blocksL) }
    return beek
  }
}

function addP (n) {
  return Math.floor(Math.random() * n)
}

function calcStat (ratingSpec, mins) {
  const { attackR, blockingR, passingR, reboundingR, stealsR } = ratingSpec

  let points = Math.floor((Math.random() * mins * attackR) / 80)

  let rebounds = Math.floor(((Math.random() * (mins * reboundingR) / (100 * 1.68))))

  let assists = Math.floor(((Math.random() * ((mins * passingR) / (100 * 1.68)))))

  let steals = Math.floor(((Math.random() * mins * stealsR)) / 500)

  let blocks = Math.floor(((Math.random() * mins * blockingR)) / 500)

  return { points,
    rebounds,
    assists,
    steals,
    blocks }
}

function average (array) {
  if (array.length === 0 || !array.length) return '0.00'
 return (array.reduce((a, b) => a + b) / array.length).toFixed(2)
}
// const oi = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35, 36, 37, 38, 39, 40, 41, 42, 43, 44, 45, 46, 47, 48, 49, 50, 51, 52, 53, 54, 55, 56, 57, 58, 59, 60, 61, 62, 63, 64, 65, 66, 67, 68, 69, 70]
