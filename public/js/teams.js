
class Team {
  constructor (teamName, teamId, short, city,
    abbrev,
    GM,
    coach,
    random,
    con,
    div,
    fun,
    selected,
    omy,
    next,
    wonTeams,
    loseTeams,
    app,
    record,
    done,
    gamesPlayed
  )
  {
    this.teamName = teamName
    this.teamId = teamId
    this.short = short
    this.city = city
    this.abbrev = abbrev
    this.players = []
    this.selected = selected ===  true
    this.set = new Set()
    this.con = con !==undefined ? con : []
    this.fun = fun !==undefined ?  fun : []
    this.div = div !==undefined ? div : []
    this.omy = omy !==undefined ? omy : []
    this.next = next !==undefined ? next : []
    this.done = done !==undefined ? done : false
    this.wonTeams = wonTeams  !== undefined ? wonTeams : []
    this.loseTeams = loseTeams !==undefined ? loseTeams : []
    this.record = record !==undefined ? record : '0.00'
    this.gamesPlayed = gamesPlayed !==undefined ? gamesPlayed : 0
    this.app = app !==undefined ? app : 0



    // this.homeFor = false
    this.GM = GM
    this.coach = coach
    this.random = random
  }
  rec () {
    if (this.gamesPlayed === 0) { return 0 } else return this.wonTeams.length / this.gamesPlayed
  }
  conf (con) {
    this.conference = con
  }

  load (arr) {
    for (let player of arr) {
      if (player.teamId === this.teamId) {
        player.tea(this)

        this.players.push(player)
      }
    }
    this.players.sort((a, b) => {
      return b.rating - a.rating
    })

    this.configureMins()
  }
  play () {
    let homePoints = this.sim()

    this.score = homePoints

    return homePoints
  }
  put (...args) {
    // console.log(team)
    for (var t of args) {
      this.set.add(t.abbrev)
      this.omy.push(t)
      t.app++
    }
  }
  dup () {
    let miles = []; let kilode = []
    for (let i = 0; i < 2; i++) {
      miles = miles.concat(this.con)
      miles = miles.concat(this.div)

      kilode = kilode.concat(this.omy.map(x => new Fixture(this, x)))
    }
    // miles = miles.concat(this.omy)
    miles = miles.concat(this.fun)

    miles = miles.map(x => new Fixture(this, x))
    // miles = miles.concat(
    // miles= this.oya(miles )

    this.li = miles
    return { fix: miles, cond: kilode }
  }

  sim () {
    let points = 0

    this.players.forEach(x => {
      if (x.playing !== 'DNP') {
        points += x.playing().points
      } else {
        points = points + 0
      }
    })

    return points
  }

  configureMins () {
    this.lineup = starting(this.players).starters
    this.lineup.forEach(x => {
      x.starting = 'Starting '
    })
    this.bench = starting(this.players).bench
    let mins = 48 * 5
    this.players.forEach(x => {
      let min = setP(x, mins)
      if (!isNaN(min))mins -= min

      // else { console.log(`${x.name} DNP ${x.rating}`) }
      // console.log(mins);
    })

    //
  }

  win (t) {
    this.wonTeams.unshift(t.teamId)
    this.gamesPlayed++
    this.update()
  }
  lose (team) {
    this.loseTeams.unshift(team.teamId)
    this.gamesPlayed++
    this.update()
  }
  update () {
    this.record = (this.wonTeams.length / this.gamesPlayed).toFixed(3)
  }
  nextT (t) {
    this.nextGame.push(t)
  }
  toJSON () {
    return {
     // $type: 'com.example.Team',
      teamName: this.teamName,
      teamId: this.teamId,
      conference: this.conference,
      city: this.city,
      wonTeams: this.wonTeams,
      random: null,
      loseTeams: this.loseTeams,
      division: this.division,
      short: this.short,

      abbrev: this.abbrev,

      selected: this.selected,

      con: this.con.map(x => x.teamId),
      fun: this.fun.map(x => x.teamId),
      div: this.div.map(x => x.teamId),
      omy: this.omy.map(x => x.teamId),
      next: this.next.map(x=>x.teamId),
      done: this.done,
      record: this.record,
      gamesPlayed: this.gamesPlayed,
      app: this.app,

      GM: this.GM,
      coach: this.coach

    }
  }
  static fromJSON (data,func) {
    const beek =  new Team(data.teamName,
      data.teamId,
      data.short,
      data.city,
      data.abbrev,
      data.GM,
      data.coach,
      data.random,
      [],
      [],
      [],
      data.selected,
      [],
      [],
      false,
      false,
      data.app,
      data.record,
      data.done,
      data.gamesPlayed)
      beek.wonTeams= data.wonTeams;
      beek.loseTeams = data.loseTeams;
      beek.record = (data.wonTeams.length /(data.wonTeams.length + data.loseTeams.length)).toFixed(3)
      return beek
  }
}

function setP (player, mins) {
  let min
  let { rating, age } = player
  if (mins > -1) {
    if (rating > 89) {
      min = Math.round(mins * 0.16 + Math.random() * 5)
    } else if (rating > 79) {
      min = Math.round(mins * 0.13 + Math.random() * 5)
    } else if (rating > 69 || age < 22) {
      min = Math.round(mins * 0.085 + Math.random() * 5)
    } else {
      min = Math.round(mins * 0.05 + Math.random() * 5)
    }
    player.addMins(min)
    return min
  } else {
    player.addMins('DNP')
    return 'DNP'
  }
}

function starting (players) {
  const Pgraph = Object.create(null)
  startGraph(Pgraph)

  for (let player of players) {
    const { position } = player

    switch (position) {
      case 'C': Pgraph['C'].push(player)
        break
      case 'PF': Pgraph['PF'].push(player)
        break
      case 'SG': Pgraph['SG'].push(player)
        break
      case 'SF': Pgraph['SF'].push(player)
        break
      case 'PG': Pgraph['PG'].push(player)
        break
    }
  }
  // console.log(Pgraph);
  const returnV = removeW(Pgraph)
  const Bench = returnV.bench
  const Starters = returnV.starters

  return { bench: Bench, starters: Starters }
}

function sortList (l) {
  return l.sort((a, b) => {
    return b.rating - a.rating
  })
}

function startGraph (graph) {
  graph['C'] = []
  graph['PF'] = []
  graph['SF'] = []

  graph['PG'] = []
  graph['SG'] = []
}

function removeW (graph) {
  let starters = []; let bench = []
  for (let key of Object.keys(graph)) {
    starters.unshift(graph[key].shift())
    graph[key].forEach(x => {
      bench.push(x)
    })
  }
  bench = sortList(bench)
  starters = sortList(starters)
  return { starters: starters, bench: bench }
}
