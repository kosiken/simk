class League {
// eslint-disable-next-line indent
    constructor (i) {
    this.name = 'SIM-K'
    this.season = i
    this.conferences = undefined

    this.count = 0
    this.fixturesCount = 1
    // if (args) this._init(args)
  };
  toJSON () {
    return {
      
      $type: 'com.example.Car',
      id: this.id,
      season: this.season,
      conferences: {Western:this.conferences.Western.teams.map(x=>x.teamId),Eastern: this.conferences.Eastern.teams.map(x=>x.teamId)},
      fixtures: this.fixtures,
      random: this.random,
      commissioner: this.commissioner,
      count: this.count,
      teams: this.teams,
    players: this.players
    }
  }
  static fromJSON (data) {
    const t = data.teams.map(Team.fromJSON); const p = data.players.map(Player.fromJSON)
    t.forEach((x, i) => {
      x.load(p)
    })
    const beek = new League(data.season); beek.commissioner = data.commissioner
    beek.random = data.random; beek.fixtures = data.fixtures.map((x, i) => {
      return Fixture.fromJSON(x, find, t)
    })
    beek.teams = t; beek.conferences = { Western: Conference.fromJSON({teams:data.conferences.Western, name:'Western'}, find, t),
      Eastern: Conference.fromJSON({teams:data.conferences.Eastern, name: 'Eastern' }, find, t) }; beek.fixturesCount = data.count
    beek.players = p; beek.random = data.random
    beek.calen(); beek.foo(); beek.count = data.count
    return beek
  }
  foo(){
    this.bar = true
  }

  init (teamList, sel, rand, p, e = false,id= '000Admin') {
    this.players = p
    this.commissioner = 'Richard Graveon'
    this.random = rand
    this.id = id
    teamList.forEach((x, i) => {
      if (x.selected || sel === x.teamName) x.selected = true
      
    })
    createConferences(this, teamList)
    createDivisions(this)
    this.onre()
    if(!this.bar)    this.createFixtureList(this.conferences)

    // if (!e) ner()
  }
  onre () {
    const { Eastern: { teams: east }, Western: { teams: west } } = this.conferences

    let been = west.slice(0, 15)
    east.forEach(x => {
      been.push(x)
    })
    this.teams = been
    // return been
  }
  createFixtureList () {
    // this.matchList =
    this.fixtures = defList(this.conferences)

    this.calen()
    // .reverse())
    // this.fixturesCount = this.matchList.length
    // this.getNext()
  }
  getNext () {
    const {den} = this.calender.weeks[this.count]
    
    
          if(den[0].done){
      this.count++
       return this.getNext()}
       else{
      this.count++ ; this.fixturesCount = 0
       return den}
    
  }
  stand () {
    let mim; let { Eastern: { teams: east }, Western: { teams: west } } = this.conferences
    mim = east.concat(west)
    mim.sort((a, b) => {
      return b.rec() - a.rec()
    })
    east.sort((a, b) => {
      return b.rec() - a.rec()
    })
    west.sort((a, b) => {
      return b.rec() - a.rec()
    })
    return { l: mim, e: east, w: west }
  }
  calen () {
    this.calender = new Calender(this.fixtures)
  }
}
class Conference {
  constructor (name, teams) {
    this.name = name
    this.divisions = 'NULL'
    this.teams = teams
  }
  toJSON () {
    return {
     // $type: 'com.example.Conference',
      name: this.name,
      teams: this.teams.map(x => x.teamId)

    }
  }

  static fromJSON (data, func = find, list) {
    return new Conference(
      data.name, data.teams.map(x => func(y => y.teamId === x, list))
    )
  }

  init () {
    this.allStars = []
    this.teams.forEach(x => {
      x.players.forEach(y => {
        if (y.rating > 91) {
          this.allStars.push(y)
        }
      })
    })
  }
}

class MatchDay {
  constructor (fixtures) {
    this.fixtures = fixtures
  }
  match () {
    return this.fixturearray.pop()
  }
}
class Fixture {
  constructor (home, away, veen) {
    this.home = home
    this.away = away
    //this.score = obj
  
    if (home.conference === away.conference) this.con = home.conference
    if (!veen) this.init(home.abbrev, away.abbrev)
    else {
      this.id = veen,
      this.sort = veen.l
    }
  }
  init (h, a) {
    if (a !== undefined) {
      this.sort = `#${h + a}`
      this.id = { l: `#${h + a}`, r: `#${a + h}` }
      this.arr = 0
      for (var i = 0; i < h.length; i++) {
        this.arr += a.charCodeAt(i)
        this.arr += h.charCodeAt(i)
      }
      
    }
  }
   
  sime () {
    this.delete = true
  }
  upload (a, b) {
    this.score = { home: a, away: b }
  }
  toJSON () {
    return {
     // $type: 'com.example.Fixture',
      home: this.home.teamId,
      away: this.away.teamId,
      id: this.id,
      score: this.score
    }
  }
  static fromJSON (data, func = find, list) {
    const beek = new Fixture(
      func(x => x.teamId === data.home, list),
      func(x => x.teamId === data.away, list), data.id
    )
    beek.score = data.score
    return beek
  }
}
class Division {
  constructor (name, teams) {
    this.name = name
    this.teams = teams
  }
}
class Calender {
  constructor (fixturearray) {
    this._init(fixturearray)
  }
  _init (f) {
    this.weeks = oya(f)
    // hen(this.weeks)
    // this.disp()
  }
}
