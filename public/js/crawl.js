class Team {
  constructor (

    teamName,
    teamId,
    short,
    city,
    abbrev,
     GM, 
     coach,
      random,
      con,
      div,
      selected,
      omy,
      next,
      wonTeams,
      loseTeams,
      app,
      done,
      gamesPlayed


  ) {
    this.teamName = teamName
    this.teamId = teamId
    this.short = short
    this.city = city
    this.abbrev = abbrev
    this.players = []
    this.selected = false
    this.set = new Set()
    this.con = []
    this.fun = []
    this.div = []
    this.omy = []
    this.next = []
    this.done = false
       this.wonTeams = []
    this.loseTeams = []
    this.record = '0.00'
    this.gamesPlayed = 0
    this.app = 0
    if(!con){
      
    }

 
    //this.homeFor = false
    this.GM = GM
    this.coach = coach
    this.random = r
  }
  rec () {
    if (this.gamesPlayed === 0) { return 0 } else return this.wonTeams.length / this.gamesPlayed
  }
  conf (con) {
    this.conference = con
  }

  load (arr) {
    for (let player of arr) {
      if (player.teamId === this.teamId) {
        player.tea(this)

        this.players.push(player)
      }
    }
    this.players.sort((a, b) => {
      return b.rating - a.rating
    })

    this.configureMins()
  }
  play () {
    let homePoints = this.sim()

    this.score = homePoints

    return homePoints
  }
  put (...args) {
    // console.log(team)
    for (var t of args) {
      this.set.add(t.abbrev)
      this.omy.push(t)
      t.app++
    }
  }
  dup () {
    let miles = []; let kilode = []
    for (let i = 0; i < 2; i++) {
      miles = miles.concat(this.con)
      miles = miles.concat(this.div)

      kilode = kilode.concat(this.omy.map(x => new Fixture(this, x)))
    }
    // miles = miles.concat(this.omy)
    miles = miles.concat(this.fun)

    miles = miles.map(x => new Fixture(this, x))
    // miles = miles.concat(
    // miles= this.oya(miles )

    this.li = miles
    return { fix: miles, cond: kilode }
  }

  sim () {
    let points = 0

    this.players.forEach(x => {
      if (x.playing !== 'DNP') {
        points += x.playing().points
      } else {
        points = points + 0
      }
    })

    return points
  }

  configureMins () {
    this.lineup = starting(this.players).starters
    this.lineup.forEach(x => {
      x.starting = 'Starting '
    })
    this.bench = starting(this.players).bench
    let mins = 48 * 5
    this.players.forEach(x => {
      let min = setP(x, mins)
      if (!isNaN(min))mins -= min

      // else { console.log(`${x.name} DNP ${x.rating}`) }
      // console.log(mins);
    })

    //
  }

  win (t) {
    this.wonTeams.unshift(t.teamId)
    this.gamesPlayed++
    this.update()
  }
  lose (team) {
    this.loseTeams.unshift(team.teamId)
    this.gamesPlayed++
    this.update()
  }
  update () {
    this.record = (this.wonTeams.length / this.gamesPlayed).toFixed(3)
  }
  nextT (t) {
    this.nextGame.push(t)
  }
  toJSON () {
    return {
      $type: 'com.example.Team',
      teamName: this.teamName,
      teamId: this.teamId,
      conference: this.conference,
      city: this.city,
      wonTeams: this.wonTeams,
      players: [],
      random: null,
      loseTeams: this.loseTeams,
      division: this.division,
      short: this.short,

      abbrev: this.abbrev,

      selected: this.selected,

      con: this.con.map(x => x.teamId),
      fun: this.fun.map(x => x.teamId),
      div: this.div.map(x => x.teamId),
      omy: this.omy.map(x => x.teamId),

      done: this.done,
      record: this.record,
      gamesPlayed: this.gamesPlayed,
      app: this.app,

      GM: this.GM,
      coach: this.coach

    }
  }
  static fromJSON (data) {
    return new Team(data.teamName,
      data.teamId,
      data.short,
      data.city,
      data.abbrev,
      data.players,
      data.selected,
      data.conference,
      data.con,
      data.fun,
      data.div,
      data.omy,
      data.division,
      data.done,
      data.wonTeams,
      data.loseTeams,
      data.record,
      data.gamesPlayed,
      data.app,

      data.GM,
      data.coach,
      data.random)
  }
}