class Loader {
  constructor (...args) {
    if (args.length > 0) {
      args.forEach(console.log)
    } else {
      console.log('')
    }
  }

  async start () {
    let players, teams

    players = await fetch('./players').then(

      x => {
        return x.json()// .then(val=>{
        //   return val;
        //    // console.log(players );

        // })
      }
    )
    const Randy = await fetch('./rand').then(x => {
      return x.json()
    })
    teams = await fetch('./teams').then(x => {
      return x.json()
    })
    let bengin = 554
    const teamsCon = []

    const playersCon = []
    players.forEach((ele, i) => {
      const { firstName, lastName,
        rating, age, position,
        height, teamId, teamName, ratingSpec } = ele
      playersCon.push(new Player(firstName, lastName,
        rating, age, position,
        height, teamId, teamName, ratingSpec, (i + bengin)))
    })
    teams.forEach(function (ele) {
      const { teamName,
        teamId,
        short,
        city,
        abbrev
        , GM, coach
      } = ele
      teamsCon.push(new Team(teamName,
        teamId,
        short,
        city,
        abbrev, GM, coach, Randy
      ))
    })
    teamsCon.forEach(x => {
      x.load(playersCon)
    })
    return { playersCon, teamsCon, Randy }
  }
}
