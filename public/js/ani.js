const container = document.querySelector('#game')
const gamesAni = popmotion.styler(document.querySelector('#cal'))
const { keyframes, easing, styler,tween } = window.popmotion
tween({
  from: { scale: 0.3

  },
  to: {
    scale: 1
  },
  duration: 1000
}).start(gamesAni.set)
// class Sim {
//     constructor() {
//         //
//     }
// }

function anim (elle, vals, property,ti=1000,opo) {
 
// else  {
  const ele = styler(elle)

  keyframes({
    values: vals,
    duration:ti,
    loop: opo,
    ease: easing.linear

  }).start(ele.set(property))
}

function anims(a,b,c) {
  let moon = a.map(styler),
  animi = Array(moon.length).fill( keyframes({
    values: b,
    duration:500,
   //loop: ti
   ease: easing.linear

  }))
popmotion.stagger(animi,300).start((v)=>v.forEach((x,
  i) => moon[i].set( c,x )))
}