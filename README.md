
# sim-k

SIMK is one of my ongoing personal projects influenced by the NBA. It uses plain javascript to simulate a basketball season
![SIMK](SIMK.png)


## IMPORTANT 
I forgot to add a correct link to the game, so to access it click on the 'SIMK' link beside the "terms of use" link. and also the login, register and other db operations dont work on heroku only if you run it locally 
and uncomment 
 ```js
 mongoose.connect('mongodb://localhost:27017/sim-k', { useNewUrlParser: true }).then(db => {console.log('CONNECTED')}).catch(err => {console.error(err)})
 ```

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes. The full functionalities are currently being tested

### Prerequisites

* [Nodejs](https://nodejs.org/en/)
* Good Browser eg: Google Chrome

### Installing

Clone the repo and then run npm install to install dependencies

Install the module

```bash
npm install
```


## Built With
* [Nodejs](https://nodejs.org/en/)
* [Express](http://www.dropwizard.io/1.0.2/docs/) - The web framework used
* [npm](https://npmjs.com) - Dependency Management


## Contributing

Please feel free to relate your insights to me


## Authors

* *Allison Kosy*



## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* Marjin Haverbaeke
* Edwin Diaz
* Stack Overflow